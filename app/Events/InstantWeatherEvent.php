<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class InstantWeatherEvent implements ShouldBroadcast
{
    use SerializesModels;
    public $weather;

    /**
     * Create a new event instance.
     *
     * @param $weather
     */
    public function __construct($weather)
    {
        $this->weather = $weather;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('weather');
    }
}
