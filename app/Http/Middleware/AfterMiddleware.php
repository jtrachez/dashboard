<?php

namespace App\Http\Middleware;

use Closure;

class AfterMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        \Artisan::call('newsfeed:get');
        \Artisan::call('weather:instant');
        return $response;
    }
}