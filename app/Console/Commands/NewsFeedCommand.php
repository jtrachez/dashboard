<?php

namespace App\Console\Commands;

use App\Events\NewsFeedEvent;
use Illuminate\Console\Command;

class NewsFeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsfeed:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get news feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $data = [];

        $items = app('Feeds')
            ->make(config('services.newsfeed.lemonde.rss'), 5)
            ->get_items();

        foreach ($items as $item) {
            $data[] = [
                'title' => $item->get_title(),
                'content' => $item->get_description(),
            ];
            dump($item->get_url());
        }

        broadcast(new NewsFeedEvent(collect($data)->take(5)));

        return $this->info('Feed rss done !');
    }
}
