<?php

namespace App\Console\Commands;

use App\Events\InstantWeatherEvent;
use App\Weathers\InstantWeather;
use Illuminate\Console\Command;

class AllCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'events:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'All events command';
    /**
     * @var InstantWeather
     */
    private $weather;

    /**
     * Create a new command instance.
     *
     * @param InstantWeather $weather
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('newsfeed:get');
        $this->call('weather:instant');
    }
}
