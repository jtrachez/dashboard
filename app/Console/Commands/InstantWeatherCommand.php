<?php

namespace App\Console\Commands;

use App\Events\InstantWeatherEvent;
use App\Weathers\InstantWeather;
use Illuminate\Console\Command;

class InstantWeatherCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:instant';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get instant weather';
    /**
     * @var InstantWeather
     */
    private $weather;

    /**
     * Create a new command instance.
     *
     * @param InstantWeather $weather
     */
    public function __construct(InstantWeather $weather)
    {
        $this->weather = $weather;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $weather = $this->weather->getWeather('currently');
        broadcast(new InstantWeatherEvent($weather));
        return $this->info('Instant Weather - Done !');
    }
}
