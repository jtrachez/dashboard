<?php
namespace App\Weathers;


use GuzzleHttp\Client;

class InstantWeather
{

    public function getWeather($key)
    {
        $response = (new Client())
            ->get(config('services.weather.api') . '/' . config('services.weather.apiKey') . '/' . config('services.weather.lat') . ',' . config('services.weather.lng'))
            ->getBody()
            ->getContents();

        return array_get(json_decode($response, true), $key);
    }
}