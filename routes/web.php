<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware(\App\Http\Middleware\AfterMiddleware::class);



Route::get('/test', function (\App\Weathers\InstantWeather $weather) {
    $w = $weather->getWeather('currently');
    dd($w);
});