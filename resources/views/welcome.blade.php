@extends('layouts.main')

@section('content')

    <div id="page-content">
        <div class="row">
            <div class="col-lg-3">
                <instant-weather></instant-weather>
            </div>
            <div class="col-lg-4">
                <news-feed></news-feed>
            </div>
            <div class="col-lg-2">
                <mail></mail>
            </div>
            <div class="col-lg-3"></div>
        </div>
       {{-- <div class="row">
            <div class="col-lg-2">

            </div>
            <div class="col-lg-2">

            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>--}}
    </div>

@stop