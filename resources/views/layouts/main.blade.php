<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard | Eurodrive Status API</title>
    @include('partials._head')
</head>
<body>
<div id="container" class="effect">

        @yield('content')

</div>
<script src="{{ elixir("js/app.js") }}"></script>
</body>
</html>
