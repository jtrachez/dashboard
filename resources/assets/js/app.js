
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

//noinspection JSUnresolvedVariable
import InstantWeather from './components/InstantWeather.vue';
import Mail from './components/Mail.vue';
import NewsFeed from './components/NewsFeed.vue';
import Echo from 'laravel-echo';

const app = new Vue({
    components: {
        InstantWeather,
        Mail,
        NewsFeed
    },
    created(){
        this.echo = new Echo({
            broadcaster: 'pusher',
            key: 'cb6c6b6a6f4690edf693',
            cluster: 'eu',
        });
    }
}).$mount('#container');
